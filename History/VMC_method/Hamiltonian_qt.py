import qutip
import scipy.sparse as sps
import scipy as sp
import numpy as np

class H_generator:
    'Generate large size hamiltonian'

    @staticmethod
    def H_simple(N):
        ide = qutip.qeye(2)
        sx = qutip.sigmax()
        sy = qutip.sigmay()
        sz = qutip.sigmaz()
        H = 0
        if N > 2:
            H += qutip.tensor([sx] + [ide]*(N-2) + [sx])
            H += qutip.tensor([sy] + [ide]*(N-2) + [sy])
            # H += qutip.tensor([sz] + [ide]*(N-2) + [sz])
        for i in range(N-1):
            H += qutip.tensor([ide]*(N-i-2)+[sx,sx]+[ide]*i)
            H += qutip.tensor([ide]*i+[sy,sy]+[ide]*(N-i-2))
            # H += qutip.tensor([ide]*i+[sz,sz]+[ide]*(N-i-2))
        return H

    def __init__(self, n_particles = 3):
        self.H_csr = H_generator.H_simple(n_particles).data
        self.H_csc = sps.csc_matrix(self.H_csr)
        self.H_lil = sps.lil_matrix(self.H_csc)
        self.n = n_particles

    def get_row(self, n, values = False):
        return h.H_csr.getrow(n)

    def get_col(self, n, values = False):
        return h.H_csc.getcol(n)

    def get(self, i, j, values = False):
        return self.H_lil[i, j]

#testing
'''

import time


n = 4
h = H_generator(n)

# print h.get_col(2)

# print h.get_row(2)




tick = time.time()
# for i in range(100):
print(h.get_row(2))
print time.time() - tick


# tick = time.time()
# # for i in range(100):
# print(h.H_csr.getrow(2))
# print time.time() - tick


'''



