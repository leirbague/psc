import time
import numpy as np
# from multipledispatch import dispatch
import timeit
import numpy.random as rng
import matplotlib.pyplot as plt
plt.subplot(221)
plt.plot([1,2,3,10,2],[1,2,3,4,5], marker='o', linestyle='None')
plt.subplot(222)
plt.plot([1,2,3,10,2],[1,2,3,4,5], marker='o', linestyle='None')
plt.subplot(2,2,3)
plt.plot([1,2,3,10,2],[1,2,3,4,5], marker='o', linestyle='None')
plt.subplot(2,2,4)
plt.scatter([1,2,3,10,2],[1,2,3,4,5], marker='o')
plt.show()