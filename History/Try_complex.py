import numpy as np
import numpy.random as rng
import theano
import theano.tensor as T
from qutip import *
from theano import function
from theano import shared
from matplotlib import pyplot as plt
import math

def S(n, s):  # takes a binary
    try:
        res = np.asarray(list(format(s, '0%db' % n)), dtype='float') - 0.5
        return res
    except:
        return S(n,int(s, 2))

#Spin XY model hamiltonian generator
def H_simple(N):
    sx = sigmax()
    sy = sigmay()
    ide = qeye(2)
    H = 0
    for i in range(N-1):
        H += tensor([ide]*(N - 2 - i) + [sx,sx] + [ide]*(i))
        H += tensor([ide]*(N - 2 - i) + [sy,sy] + [ide]*(i))
    if N > 2:
        H += tensor([sx] + [ide] * (N - 2) + [sx])
        H += tensor([sy] + [ide] * (N - 2) + [sy]) 
    return H.data.toarray()

#size of visible and hidden layers
n_visible = 12
n_hidden = 24

#compute all spin configurations: [000],[001],[010],[011] etc..
hilbert = np.array(list(S(n_visible, i) for i in range(2 ** n_visible)))

#generate the hamiltonian of interest
hamilton = H_simple(n_visible)
hamilton = np.real(hamilton)

#Faire n'importe quoi avec l'hamiltonian pour tester
#hamilton[1][3] = 1
#hamilton[3][1] = 1
#hamilton[4][0] = hamilton[0][4] = -3

##set of (complex) parameters that characterises the wavefunction
#w = shared(np.zeros((n_hidden, n_visible)), name = 'w')
#wi = shared(np.zeros((n_hidden, n_visible)), name = 'wi')
w = shared(rng.rand(n_hidden, n_visible) - 0.5, name = 'w')
wi = shared(rng.rand(n_hidden, n_visible) - 0.5, name = 'wi')
#a = shared(np.zeros(n_visible), name = 'a')
#ai = shared(np.zeros(n_visible), name = 'ai')
a = shared(rng.rand(n_visible), name = 'a')
ai = shared(rng.rand(n_visible), name = 'ai')
#b = shared(np.zeros(n_hidden), name = 'b')
#bi = shared(np.zeros(n_hidden), name = 'bi')
b = shared(rng.rand(n_hidden), name = 'bi')
bi = shared(rng.rand(n_hidden), name = 'bi')

#set arbitrary initial configuration for testing
#w = shared(np.array([[1,2],[4,3]]), name = 'w')
#wi = shared(np.array([[4,3],[1,2]]), name = 'wi')
#a = shared(np.array([3,7]), name = 'a')
#ai = shared(np.array([3,7]), name = 'ai')
#b = shared(np.array([-7,-7]), name = 'b')
#bi = shared(np.array([-3,-3]), name = 'bi')
#v_in = rng.rand(n_visible) - 0.5

def w_f(v_in):
    return np.exp(np.dot(a.get_value() + 1j*ai.get_value(), v_in)) * np.prod(2 * np.cosh(b.get_value() + 1j*bi.get_value() + np.dot(w.get_value() + 1j*wi.get_value(),v_in)))

#calculate the correct wavefunction and energy for testing purpose
#print("results we should obtain:")
#print("########################################")
#state = np.array(list(w_f(v) for v in hilbert))
#state /= np.linalg.norm(state)
#print(state)
#e = np.dot(np.conj(state),np.dot(hamilton, state))
#print(e)
#print("########################################")
#print("constructing wavefunction...")

#all the (ad-hoc) functions intended for manipulating complex numbers
def c_times (c1, c2):
    r_out = c1[0] * c2[0]
    t_out = (c1[1] + c2[1])
    return [r_out, t_out]

def c_add (c1, c2):
    x = c1[0] * T.cos(c1[1]) + c2[0] * T.cos(c2[1]) 
    y = c1[0] * T.sin(c1[1]) + c2[0] * T.sin(c2[1])
    r_out = T.sqrt(x * x + y * y)
    t_out = T.arctan(y / x)
    return [r_out, t_out]

def c_add_list (c_l):
    c = c_l[0]
    for i in c_l:
        c = c_add(c, i)
    return c

def c_conj (c):
    return [c[0], -c[1]]

def c_real (c):
    return c[0]*T.cos(c[1])

def c_imag (c):
    return c[0]*T.sin(c[1])

#Functions that constitute the funal wavefunction, psi
def hidden_k (v, b, bi, w, wi, k):
    r1 = T.exp(b[k] + T.dot(w[k,:], v))
    t1 = bi[k] + T.dot(wi[k,:], v)
    r2 = 1 / r1
    t2 = -t1
    return c_add([r1,t1], [r2, t2])

def hidden (v, b, bi, w, wi):
    prod = hidden_k(v, b, bi, w, wi, 0)
    for i in range(1, n_hidden):
        prod = c_times(prod, hidden_k(v, b, bi, w, wi, i))
    return prod

#wavefunction
def psi (v, b = b, bi=bi, w=w, wi=wi):
    visible = [T.exp(T.dot(a, v)), T.dot(ai, v)]
    return c_times(visible, hidden(v, b, bi, w, wi))

#calculate wavefunction at every spin configuration in hilbert
def state (n = n_visible, b = b, bi=bi, w=w, wi=wi):
    return list(psi(v, b, bi, w, wi) for v in hilbert)

v = T.vector('v')

#Construct wavefunction (in polar form) and normalise
state_final = T.stack(state(), 1)
norm = T.sqrt(T.sum(T.square(state_final[0])))

#real part of the wavefunction
state_r = state_final[0]*T.cos(state_final[1]) / norm
#imaginary part if the wavefunction
state_i = state_final[0]*T.sin(state_final[1]) / norm

#matrix for of everything
state_matrix = T.transpose(T.stack([state_r, state_i]))
f = function([], state_matrix)
init_state = f()
print(init_state)
print("calculating energy...")

#calulate <energy> = <Psi|H|Psi> = <Psi_real|H|Psi_real> + <Psi_imag|H|Psi_imag>
e_total = T.dot(state_r, T.dot(hamilton, state_r)) + T.dot(state_i, T.dot(hamilton, state_i))

#Empirical finding: better to optimise log energy to prevent overshooting
log_energy = T.log(e_total)

energy = function([], e_total)
print(energy())

############################################################################
#derivative of energy
print("Calculating gradient wrt w, a, b")
gw, gwi, ga, gai, gb, gbi = T.grad(e_total, [w, wi, a, ai, b, bi])
#d_E = function([], [gw, ga, gb])
#print(d_E())

#training
slope = 1

#print(slope)
train = function(
          [],
          outputs= e_total,
          updates=((w, w - slope * gw), (wi, wi - slope * gwi), (a, a - slope * ga), (ai, ai - slope * gai), (b, b - slope * gb), (bi, bi - slope * gbi)))

print("Initial (normalized) ground state wavefunction:")
norm = np.sum(np.sqrt(np.sum(init_state ** 2, axis = 1)))

init_state = np.transpose(init_state / norm)
print(init_state)

plt.xlabel('real part')
plt.ylabel('imaginary part')
plt.scatter(init_state[0], init_state[1], c=init_state[1], s=100, cmap='gray')
plt.scatter([0],[0], color = 'r')
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.show()

criterion_of_stopping = 1e-6
e1 = train()
progress = 0
for i in range(3000):
#    print("current progress:")
    energyTotal = train()
    progress = abs((e1 - energyTotal) / e1)
#    print(progress)
#    if progress < criterion_of_stopping or math.isnan(progress):
#        print("good approx reached")
#        break
    e1 = energyTotal

final_state = f()
print(final_state)
print("progress")
print(progress)

print("Final (normalized) ground state wavefunction, by simple gradien descend:")
norm = np.sum(np.sqrt(np.sum(final_state ** 2, axis = 1)))

final_state = np.transpose(final_state / norm)
print(final_state)

energy = function([], e_total)
print(energy())


plt.xlabel('real part')
plt.ylabel('imaginary part')
plt.scatter(final_state[0], final_state[1], c=final_state[1], s=100, cmap='gray')
plt.scatter([0],[0], color = 'r')
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.show()


plt.hist(np.sqrt(final_state[0]**2 + final_state[1]**2))
plt.show()

