import time
import numpy as np
import numpy.random as rng
import matplotlib.pyplot as plt
import cmath
import math
import cProfile
import sys

# Hamiltonian of spin xy model. n = number of particles.
# Return a dictionary of which the keys are the indices of non-empty columns in a row given. The values are the value of the corresponding entry
def hamilton(n, row):
    result = {}
    mask = 3  # binary mask = 11
    for i in range(n - 1):
        if (bool(((row >> i) ^ (row >> i + 1)) & 1)):  # check if the ith digit is different from (i + 1)th digits
            result[row ^ (mask << i)] = 2  # flip digits at i and i + 1
    if (bool((row >> (n - 1)) ^ (row & 1))):  # check the first against the last digit
        result[row ^ (1 | (1 << (n - 1)))] = 2
    return result

#to a binary array representation of the quantum state
def to_bin(n, s):
    return [((s >> i) & 1) * 2 - 1 for i in reversed(range(n))]

# given an rbm and a state, compute the corresponding effective angle
def theta(rbm, state):
    if state not in rbm['theta_table']:
        rbm['theta_table'][state] = rbm['b'] + np.dot(to_bin(rbm['n'], state), rbm['w'])
    return rbm['theta_table'][state]

# natural log of the value of the wave function at configuration 'state'
def ln_psi(rbm, state, t=None):
    if state not in rbm['ln_psi_table']:
        n = rbm['n']
        if t is None:
            t = theta(rbm, state)
        result = np.dot(rbm['a'], to_bin(n, state)) + rbm['h'] * np.log(2) + np.sum(np.log(np.cosh(t)))
        rbm['ln_psi_table'][state] = min(result, math.log(sys.float_info.max) - 1)
    # assert math.log(sys.float_info.max) - 1 > rbm['ln_psi_table'][state].real, "Wave function has exploded!"
    return rbm['ln_psi_table'][state]

# the value of the wave function at configuration 'state'
def psi(rbm, state, theta=None):
    return np.exp(ln_psi(rbm, state, theta))

# inverse the spin of a random particle and see what happens to theta (effective angle) and log psi
# if the index of the particle to flip is not given, a random particle will be flipped
def bit_flip(rbm, index_to_flip=None):
    if index_to_flip is None:
        index_to_flip = np.random.randint(rbm['n'])
    assert index_to_flip < rbm['n']
    mask = 1 << index_to_flip
    new_position = rbm['walker'] ^ mask
    digit_after_flip = int(bool(new_position & mask)) * 2 - 1
    t = rbm.get('theta') + 2 * (rbm.get('w'))[rbm.get('n') - (index_to_flip + 1), :] * digit_after_flip
    return new_position, t, ln_psi(rbm, new_position, t)

# take a random walk
def walk(rbm):
    new_position, new_theta, new_ln_psi = bit_flip(rbm)
    # assert isinstance(new_ln_psi, complex), "wave function is not a complex number"
    # p is the Metropolis transition probability between the two chosen states
    p = np.exp((new_ln_psi.real - rbm['ln_psi'].real) * 2)
    if (p >= 1) or (rng.ranf() < p):
        rbm['walker'], rbm['theta'], rbm['ln_psi'], rbm['psi'] = new_position, new_theta, new_ln_psi, np.exp(new_ln_psi)
    return rbm

# a bookkeeper
def RBM(n, h, spread=None, initial_steps=100):
    if spread is None:
        upp_bound = (math.log(sys.float_info.max) - 3) / (n + h + n * h)
    else:
        upp_bound = spread
    initial_bound = upp_bound / 100000
    # n = number of particles, h = number of hidden variables, initialise all parameters.
    # also make two table de hachage for l
    rbm = dict(n=n,
               h=h,
               a=np.random.uniform(-initial_bound, initial_bound, n) + np.random.uniform(-3.14, 3.14, n) * 1j,
               b=np.random.uniform(-initial_bound, initial_bound, h) + np.random.uniform(-3.14, 3.14, h) * 1j,
               w=np.random.uniform(-initial_bound, initial_bound, (n, h)) + np.random.uniform(-3.14, 3.14, (n, h)) * 1j,
               # a=np.random.normal(0, spread, n) + np.random.normal(0, spread, n) * 1j,
               # b=np.random.normal(0, spread, h) + np.random.normal(0, spread, h) * 1j,
               # w=np.random.normal(0, spread, (n, h)) + np.random.normal(0, spread, (n, h)) * 1j,
               bound=upp_bound,
               theta_table={},
               ln_psi_table={},
               steps=0,
               mc_energy=0)
    # store the position of the current walker
    rbm['walker'] = np.random.randint(2 ** n)
    # store the effective angle, log amplitude and actual amplitude at the current position 'state'
    rbm['theta'] = theta(rbm, rbm['walker'])
    rbm['ln_psi'] = ln_psi(rbm, rbm['walker'], rbm['theta'])
    rbm['psi'] = psi(rbm, rbm['walker'], rbm['theta'])
    # initialise to get a walker position that is random enough
    for i in range(initial_steps):
        walk(rbm)
    rbm['mc_energy'] = integrator(rbm, loc_energy)
    return rbm

def clear_hachage(rbm):
    rbm['theta_table'] = {}
    rbm['ln_psi_table'] = {}

# local energy at the current position of the Monte Carlo walker (the position is stored in the rbm dictionary)
def loc_energy(rbm):
    h_row = hamilton(rbm['n'], rbm['walker'])
    return sum([psi(rbm, j) * h_row[j] for j in h_row]) / rbm['psi']

# variational derivatives.
def var_derivatives(rbm):
    o_a = np.array(to_bin(rbm['n'], rbm['walker']))
    o_b = np.tanh(theta(rbm, rbm['walker']))
    o_w = np.outer(o_a, o_b).flatten()
    loc_E = loc_energy(rbm)
    o = np.conj(np.concatenate((o_a, o_b, o_w)))
    oo = np.outer(np.conj(o), o).flatten()
    return np.concatenate(([loc_E], o, o * loc_E, oo))

def integrator(rbm, func, steps=1000):
    return np.mean([func(walk(rbm)) for i in range(steps)], axis=0)

# determines the amount of regularisation
def lam(steps, lam_0=100, b=0.9, lam_min=0.0004):
    return max(lam_min, (b ** steps) * lam_0)

def forces(rbm, steps=1000):
    n, h = rbm['n'], rbm['h']
    n_params = n + h + n * h
    result = integrator(rbm, var_derivatives, steps)
    # unpack the results of integrator
    loc_E, o, o_times_loc_E, oo = result[0], result[1:1 + n_params], result[1 + n_params:1 + 2 * n_params], result[-n_params ** 2:]
    force = [o_times_loc_E[k] - loc_E * o[k] for k in range(n_params)]
    # print('MC energy: ', '\t', loc_E.real)
    # Compute the covariance matrix
    S_kk_1 = oo.reshape(n_params, n_params)
    S_kk_2 = np.array([[o[k1] * np.conj(o[k2]) for k1 in range(n_params)] for k2 in range(n_params)])
    S_kk = S_kk_1 - S_kk_2
    # regularisation
    S_kk_reg = (np.ones(n_params) + np.eye(n_params) * lam(rbm['steps'])) * S_kk
    # print("inverse")
    force = np.dot(np.linalg.pinv(S_kk_reg), force)
    # print("done")
    return force

# Do one iteration of gradient descend
def gradient_descend(rbm, learning_rate=0.01, steps=1000):
    n, h = rbm['n'], rbm['h']
    f = np.array(forces(rbm, steps))
    da = learning_rate * f[:n]
    db = learning_rate * f[n:(h + n)]
    dw = learning_rate * (f[(h + n):]).reshape(rbm['n'],rbm['h'])
    rbm['a'] = np.clip(rbm['a'] - da, -rbm['bound'], rbm['bound'])
    rbm['b'] = np.clip(rbm['b'] - db, -rbm['bound'], rbm['bound'])
    rbm['w'] = np.clip(rbm['w'] - dw, -rbm['bound'], rbm['bound'])
    clear_hachage(rbm)
    rbm['steps'] += 1

# for testing
def normaliser(rbm):
    norm = 0
    for i in range(2 ** rbm['n']):
        norm += np.absolute(psi(rbm, i)) ** 2
    return norm ** (0.5)

def energy_exact(rbm):
    accumulate = 0
    for i in range(2 ** rbm['n']):
        h_row = hamilton(rbm['n'], i)
        for j in h_row:
            accumulate += np.conj(psi(rbm, i)) * h_row[j] * psi(rbm, j)
    return np.real_if_close([accumulate / (normaliser(rbm) ** 2)])[0]

def show_psi(rbm, plot=False):
    p = np.array([np.absolute(psi(rbm, i)) ** 2 for i in range(2 ** rbm['n'])])
    p = p / p.sum()
    if plot:
        plt.plot(p, c='b', marker='.', linestyle='None')
    i = p.argmax()
    state = ' '.join(['+' if x > 0 else '-' for x in to_bin(rbm['n'], i)])
    # print('State with highest amplitude:\t', state)
    print(np.around(p, decimals=2))

# size = number of particles
# def row(index, size):
# 	mask = 3 # binary = 11
# 	non_zeros = []
#     for i in range(size - 1):
#         if (bool(((index >> i) ^ (index >> i + 1)) & 1)):     #check if the ith degit is different from jth digits
#             non_zeros.append(index ^ (mask << i))             # flip digits at i and i + 1
# 	if (bool((index >> (size - 1)) ^ (index & 1))):           # check the first against the last digit
# 		non_zeros.append(index ^ (1 | (1 << (size - 1))))
#     return non_zeros

# spread = 0.1
rate = 0.01
n, h = 5, 15
rbm = RBM(n, h)
print('bound: ', rbm['bound'])

print('exact energy: ', '\t',energy_exact(rbm))
print('MC energy: ','\t', np.real(integrator(rbm, loc_energy, steps=1000)))

##################################################################################################
##################################################################################################
# # test if Monte Carlo simulates the correct energy value
# # and how fast its simulated result of energy value converges to the correct exact energy value
# # Test result is good
# e=[]
# mc_e =[]
# r = [i for i in range(1000, 100000, 1000)]
# for k in range(1):
#     rbm = RBM(n, h, spread)
#     energy = energy_exact(rbm)
#     e.append([energy] * len(r))
#     mc_e.append([integrator(rbm, loc_energy, steps=j) for j in r])
# e = np.array(e)
# mc_e = np.array(mc_e)
# print("done")
#
# error = e - mc_e
# # error1 = np.around(np.absolute(error), decimals=2)
# # error2 = np.around(np.imag(error), decimals=2)
# # error3 = np.around(np.real(error), decimals=2)
#
# error1 = np.absolute(error)[0]
# error2 = np.imag(error)[0]
# error3 = np.real(error)[0]
#
#
# # plt.figure(1)
# plt.plot(r, error1, r, error2, r, error3)
# # print(error1, '\n', error2, '\n', error3)
#
# plt.show()
##################################################################################################
##################################################################################################
# get the hamiltonian matrix (danger!)
ham = np.array([[2 if i in hamilton(n, j) else 0 for i in range(2 ** n)] for j in range(2 ** n)])


# get explicit wavefunction
wf = np.array([psi(rbm, i) for i in range(2 ** n)])

p = np.absolute(wf) ** 2
p = p / np.sum(p)

##################################################################################################
##################################################################################################
# the exact eigenstates and energies
eigenSys = np.linalg.eigh(ham)
eigenVal = np.around(eigenSys[0], decimals=2)
eigenVec = np.around(eigenSys[1], decimals=2)
e0 = eigenVal[0]
# print(ham)
# print(eigenVal, '\n', eigenVec)

##################################################################################################
##################################################################################################
# # plot ground states
# indices = [i for i in range(2 ** n) if eigenVal[i] == e0]
# # print(indices)
# plt.figure(1)
# plt.gcf().canvas.set_window_title('Energy = {}'.format(np.around(e0, decimals=3)))
# for i in indices:
#     plt.subplot(len(indices), 1, i + 1)
#     plt.plot(eigenVec[i])
# # plt.show()

##################################################################################################
##################################################################################################
# # test if the walker walks correctly
# cnt = [0 for i in range(2 ** n)]
# for i in range(100000):
#     walk(rbm)
#     cnt[rbm['walker']] += 1
# cnt = np.array(cnt)
# cnt = cnt / (cnt.mean() * (2 ** n))
# plt.figure(1)
# plt.subplot(311)
# plt.plot(cnt, c='r', marker='.', linestyle='None')
# plt.subplot(312)
# plt.plot(p,  c='b', marker='.', linestyle='None')
# plt.subplot(313)
# plt.plot(p - cnt,  c='g', marker='.', linestyle='None')
# plt.show()
##################################################################################################
##################################################################################################

plt.figure(2)
print('ground: ', eigenVec[0])

plot=True
print("initially:")
show_psi(rbm, plot=plot)
# plt.show()
diff = 0
print('target ground state energy:\t', e0)
print('\nEnergy in each iteration:')
for i in range(10):
    diff = abs(energy_exact(rbm) - e0)
    if diff < 0.001:
        break
    gradient_descend(rbm, learning_rate=1, steps=1000)
    show_psi(rbm, plot=False)
    # print('{}:\t'.format(i), 'energy: ', '\t', np.around(energy_exact(rbm), decimals= 3), '\t diff:\t', np.around(diff, decimals=3))

print()
print('exact energy: ' ,energy_exact(rbm))
print('MC energy: ','\t', np.real(integrator(rbm, loc_energy)))

print("and finally: ")
show_psi(rbm, plot=plot)
#
# plt.show()


