# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 00:14:49 2016

@author: Yunzhe
"""

import numpy as np
import numpy.random as rng
import theano
import theano.tensor as T
from qutip import *
from theano import function
from theano import shared

#rng.seed(2)

#Spin XY model hamiltonian generator
def H_simple(N):
    sx = sigmax()
    sy = sigmay()
    ide = qeye(2)
    H = 0
    for i in range(N-1):
        H += tensor([ide]*(N - 2 - i) + [sx,sx] + [ide]*(i))
        H += tensor([ide]*(N - 2 - i) + [sy,sy] + [ide]*(i))
    if N > 2:
        H += tensor([sx] + [ide] * (N - 2) + [sx])
        H += tensor([sy] + [ide] * (N - 2) + [sy]) 
    return H.data.toarray()

#size of visible and hidden layers
n_visible = 3
n_hidden = 3

#compute all spin configurations: [000],[001],[010],[011] etc..
hilbert = []
hilbert_size = 2**n_visible
for i in range(hilbert_size):
    spin_config = map(int, list(str(bin(i))[2:]))
    #fill up with zeros
    spin_config = [0]*(n_visible - len(spin_config)) + spin_config
    hilbert.append(np.array(spin_config))
hilbert = np.array(hilbert)

#generate the hamiltonian of interest
hamilton = H_simple(n_visible)

#set of (complex) parameters that characterises the wavefunction
w = rng.rand(n_hidden, n_visible) + 1j * rng.rand(n_hidden, n_visible)
a = rng.rand(n_visible) + 1j * rng.rand(n_visible)
b = rng.rand(n_hidden) + 1j * rng.rand(n_hidden)
params = [w, a, b]
#print(w)
print(a)



#wavefunction amplitude at [v]
def psi(v, params = params):
    return np.exp(np.dot(params[1], v)) * np.prod(2 * np.cosh(params[2] + np.dot(params[0],v)))

#construct energy function
def energy(hilbert = hilbert, hamilton = hamilton, params = params):
    #compute the amplitude of the wavefunction at each spin configuration
    amp = []
    for i in hilbert:
        amp.append(psi(i, params))
    amp = np.array(amp)
    #print(amp)
    return np.dot(np.dot(amp.conj(), hamilton), amp)
    #print(energy)

def walk(params):
    #random walk
    pace = 1.0
    w_step = pace * (rng.rand(n_hidden, n_visible) - 0.5)
    a_step = pace * (rng.rand(n_visible) - 0.5)
    b_step = pace * (rng.rand(n_hidden) - 0.5)
    w_step_i = pace * (rng.rand(n_hidden, n_visible) - 0.5)
    a_step_i = pace * (rng.rand(n_visible) - 0.5)
    b_step_i = pace * (rng.rand(n_hidden) - 0.5)
    return [, a_step, b_step], [1j * w_step_i, 1j*a_step_i, 1j * b_step_i]]
#
#interation = 5
#while(interation > 0):
#    step = walk();
#    e_grad_r = (energy(hilbert, hamilton, params + step[0]) - energy(hilbert, hamilton, params)) / step[0]
#    e_grad_i = (energy(hilbert, hamilton, params + step[1]) - energy(hilbert, hamilton, params)) / step[1]
#    params += (e_grad_r + 1j * e_grad_i) * 10
#    interation += 1

step = walk();
print("original")
print(params)
print("step")
print(step)
print("modified param")
print(params + step[0])
print("yishang")
e_grad_r = (energy(hilbert, hamilton, params + step[0]) - energy(hilbert, hamilton, params))
e_grad_i = (energy(hilbert, hamilton, params + step[1]) - energy(hilbert, hamilton, params))
params += e_grad_r * step[0] + 1j * e_grad_i * step[1] 
print("grad")
print(e_grad_r)
print(a)
print("c'est fini !")

##debug and testing
#print("for testing: ")
#test = 5
#print("w:")
#print(w.get_value())
#print("a:")
#print(a.get_value())
#print("b:")
#print(b.get_value())
#print(hilbert[test])
#print(wavefunction(hilbert[test]))
#print(energy())
#print("testing ended")

#test_w = np.array(w.get_value())
#test_a = np.array(a.get_value())
#test_b = np.array(b.get_value())
##print(test_w)
##print(test_a)
#
##hilbert = np.array([[0, 0],[0,1],[1,0],[1,1]])
##test_w
#
#res = []
#for sigma in hilbert:
#    sigma = np.array(sigma)
#    res.append(np.exp(np.dot(test_a, sigma)) * np.prod(2 * np.cosh(test_b + np.dot(test_w, sigma))))
#    
#res = np.array(res)
#ener = np.dot(np.dot(res.conj(),hamilton), res)
#print(ener)
#
#def gradient(func = None):
#    func()
#    
#    return func()
#
#print(gradient(energy))

