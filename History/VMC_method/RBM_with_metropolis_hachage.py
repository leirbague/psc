'''
using variantional monte carlo (VMC) methode to attempt a faster solution of the ground state of Spin XY - problem
under construction...
'''

import numpy as np

def revert_binary(bitlist):
    output = 0
    bitlist = list(((int(i) + 1)/2) for i in bitlist)
    for bit in bitlist:
        output = (output << 1) | bit
    return output

class RBM:
    'Restricted boltzmann machine with metropolis algorithm incorporated'

    def S(self, s):
        out = []
        for i in range(self.n):
            num = s >> i
            digit = (num & 1) * 2 - 1
            out = [digit] + out
        return out

    # Hamiltonian of spin xy model. size = number of particles
    def row(self, index):
        mask = 3  # binary = 11
        non_zeros = []
        for i in range(self.n - 1):
            if (
            bool(((index >> i) ^ (index >> i + 1)) & 1)):  # check if the ith degit is different from (i + 1)th digits
                non_zeros.append(index ^ (mask << i))  # flip digits at i and i + 1
        if (bool((index >> (self.n - 1)) ^ (index & 1))):  # check the first against the last digit
            non_zeros.append(index ^ (1 | (1 << (self.n - 1))))
        return non_zeros

    #effective angle
    def theta(self, basis):
        if self.hachage:
            if isinstance(basis, int):
                basis_as_int = basis
                basis = self.S(basis)
            else:
                basis_as_int = revert_binary(basis)
            record = self.effective_angle_table.get(basis_as_int)
            if record is not None:
                return record
            record = self.b + np.dot(self.w, basis)
            # self.theta_computation += 1
            self.effective_angle_table[basis_as_int] = record
            return record
        else:
            # self.theta_computation += 1
            if isinstance(basis, int):
                basis = self.S(basis)
            return self.b + np.dot(self.w, basis)

    #natural log of the value of the wave function at configuration 'basis'
    def ln_psi(self, basis, theta = None):
        if self.hachage:
            if isinstance(basis, int):
                basis_as_int = basis
                basis = self.S(basis)
            else:
                basis_as_int = revert_binary(basis)
            record = self.ln_psi_table.get(basis_as_int)
            if record is not None:
                return record
            #number of hidden variables
            m = self.b.shape[0]
            ln_2 = np.log(2)
            if theta is None:
                t = self.theta(basis)
            else:
                t = theta
            record = np.dot(self.a, basis) + m * ln_2 + np.sum(np.log(np.cosh(t)))
            self.ln_psi_table[basis_as_int] = record
            return record
        else:
            if isinstance(basis, int):
                basis = self.S(basis)
            m = self.b.shape[0]
            ln_2 = np.log(2)
            if theta is None:
                t = self.theta(basis)
            else:
                t = theta
            return np.dot(self.a, basis) + m * ln_2 + np.sum(np.log(np.cosh(t)))

    #the value of the wave function at configuration 'basis'
    def psi(self, basis, theta = None):
        if self.hachage:
            if isinstance(basis, int):
                basis_as_int = basis
                basis = self.S(basis)
            else:
                basis_as_int = revert_binary(basis)
            record = self.psi_table.get(basis_as_int)
            if record is not None:
                return record
            record = np.exp(self.ln_psi(basis, theta))
            self.psi_table[basis_as_int] = record
            return record
        else:
            return np.exp(self.ln_psi(basis, theta))

    #indices counting from the right
    def flip(self, i):
        if isinstance(i, list):
            return self.multi_flip(i)
        theta = self.current_effective_angle
        new_basis = self.current_basis
        mask = 1 << i
        new_basis = new_basis ^ mask
        digit = int(bool(new_basis & mask)) * 2 - 1
        theta = theta + 2 * self.w[:, self.n - (i + 1)] * digit
        psi = self.ln_psi(new_basis, theta)
        return new_basis, theta, psi

    def multi_flip(self, indices):
        theta = self.current_effective_angle
        newState = self.current_basis
        for i in indices:
            flipper = 1 << i
            newState = newState ^ flipper
            digit = int(bool(newState & flipper)) * 2 - 1
            theta = theta + 2 * self.w[:, self.n - (i + 1)] * digit
        psi = self.ln_psi(newState, theta)
        return newState, theta, psi

    #take a random step
    def walk(self):
        index_to_flip = np.random.randint(self.n)
        new_basis, newTheta, changed_ln_psi = self.flip(index_to_flip)
        dice = np.random.ranf()
        if ((changed_ln_psi > self.current_ln_psi) or (dice < np.exp(2 * (changed_ln_psi - self.current_ln_psi)))):
            self.current_effective_angle, self.current_basis, self.current_ln_psi = newTheta, new_basis, changed_ln_psi
            return self.current_basis, self.current_ln_psi, self.current_effective_angle
        else:
            return self.current_basis, self.current_ln_psi, self.current_effective_angle

    #initialise the metropolis algorithms.
    def init_metropolis(self, initial_steps = 10):
        self.current_basis = np.random.randint(2 ** self.n)  # an integer
        self.current_effective_angle = self.theta(self.current_basis)
        self.current_ln_psi = self.ln_psi(self.current_basis, self.current_effective_angle)
        for i in range(initial_steps):
            self.walk()

    def energy(self, steps = 1000):
        accumulate = 0
        for i in range(steps):
            basis, ln_psi, theta = self.walk()
            psi = np.exp(ln_psi)
            temp = 0  #local energy at "basis"
            for j in self.row(basis):
                # flipper = j ^ basis
                # indices = []
                # for i in range(self.n):
                #     if bool(flipper & (1 << i)):
                #         indices.append(i)
                # # print "i, j = {},{}".format(i,j)
                # temp += np.exp(self.flip(indices)[2])
                # l'autre facon
                temp += self.psi(j)
            accumulate += (temp / psi)
        return 2 * accumulate / steps

    # def covariance_matrix(self):


    # def energy_flip(self, steps = 1000):
    #     accumulate = 0
    #     for i in range(steps):
    #         basis, ln_psi, theta = self.walk()
    #         psi = np.exp(ln_psi)
    #         temp = 0  #local energy at "basis"
    #         for j in self.row(basis):
    #             flipper = j ^ basis
    #             indices = []
    #             for i in range(self.n):
    #                 if bool(flipper & (1 << i)):
    #                     indices.append(i)
    #             # print "i, j = {},{}".format(i,j)
    #             temp += np.exp(self.flip(indices)[2])
    #             # l'autre facon
    #             # temp += self.psi(j)
    #         accumulate += (temp / psi)
    #     return 2 * accumulate / steps

    def normaliser(self):
        norm = 0
        for i in range(2 ** self.n):
            norm += self.psi(i)**2
        return norm ** (0.5)

    def energy_exact(self):
        accumulate = 0
        for i in range(2 ** self.n):
            for j in self.row(i):
                accumulate += self.psi(i) * 2 * self.psi(j)
        return accumulate / (self.normaliser() ** 2)

    def __init__(self, a, b, w, n, hachage = False):
        self.n = n
        self.a = a
        self.b = b
        self.w = w
        self.random_walking = False
        self.hachage = hachage
        if self.hachage:
            self.psi_table = {}
            self.ln_psi_table = {}
            self.effective_angle_table = {}
            # self.theta_computation = 0


