
from qutip import *

def H_simple(N):
    sx = sigmax()
    sy = sigmay()
    ide = qeye(2)
    H = 0
    for i in range(N-1):
        H += tensor([ide]*(N - 2 - i) + [sx,sx] + [ide]*(i))
        H += tensor([ide]*(N - 2 - i) + [sy,sy] + [ide]*(i))
    if N > 2:
        H += tensor([sx] + [ide] * (N - 2) + [sx])
        H += tensor([sy] + [ide] * (N - 2) + [sy]) 
    return H.data

n = int(raw_input('Input number of particles:'))

print(H_simple(n))