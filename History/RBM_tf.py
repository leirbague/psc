""" An rbm implementation for TensorFlow, based closely on the one in Theano """
import tensorflow as tf
import math

def sample_prob(probs):
    return tf.nn.relu(
        tf.sign(
            probs - tf.random_uniform(probs.get_shape())))

def random_complex_matrix(n,m,std):
    return tf.truncated_normal([n, m], stddev = std) + 1j*tf.truncated_normal([n, m], stddev = std)

class RBM(object):
    def __init__(
        self,
        name,
        n_spin = 10,
        m_hidden = 40,
        s_bias = None,
        h_bias = None,
        W = None,
        ):
        with tf.name_scope("rbm_" + name):
            if not W:
                W = tf.Variable(random_complex_matrix(n_spin, m_hidden, 1.0 / math.sqrt(float(n_spin))),name="W")
            if not s_bias:
                s_bias = tf.Variable(tf.zeros([n_spin]), name="s_bias")
            if not h_bias:
                tf.Variable(tf.zeros([m_hidden]), name="h_bias")
            self.W = W  # weight matrix (= W_ij)
            self.s_bias = s_bias # biases of spin vatiables ("phases/amplitudes"= a_i)
            self.h_bias = h_bias # biases of hidden variables (= b_j)
            self.params = [self.W, self.h_bias, self.s_bias]

    def propup(self, visible):
        return tf.nn.sigmoid(tf.matmul(visible, self.W) + self.h_bias)

    def propdown(self, hidden):
        return tf.nn.sigmoid(tf.matmul(hidden, tf.transpose(self.W)) + self.s_bias)

    def sample_h_given_s(self, s_sample):
        return sample_prob(self.propup(s_sample))

    def sample_s_given_h(self, h_sample):
        return sample_prob(self.propdown(h_sample))

    def gibbs_hsh(self, h0_sample):
        s_sample = self.sample_s_given_h(h0_sample)
        h_sample = self.sample_h_given_s(s_sample)
        return [s_sample, h_sample]

    def gibbs_shs(self, s0_sample):
        h_sample = self.sample_h_given_s(s0_sample)
        s_sample = self.sample_s_given_h(h_sample)
        return [h_sample, s_sample]

    def F(self,i,s_sample):
        return 2*

    def wave_function(self,s_sample):
        tf.matmul(tf.transpose(s_sample), self.s_bias)
        return

    def local_energy(self,S_input):

    def variational_derivatives(self,input):

    def cost(self, visible_spins, learning_rate=0.1): # TODO: EDIT THIS WITH THE PAPER'S FUNCTIONS 'N VARIABLES
        h_start = self.propup(visible_spins)
        s_end = self.propdown(h_start)
        h_end = self.propup(s_end)
        w_positive_grad = tf.matmul(tf.transpose(visible_spins), h_start)
        w_negative_grad = tf.matmul(tf.transpose(s_end), h_end)
        update_w = self.W.assign_add(learning_rate * (w_positive_grad - w_negative_grad))
        update_sb = self.s_bias.assign_add(learning_rate * tf.reduce_mean(visible_spins - s_end, 0))
        update_hb = self.h_bias.assign_add(learning_rate * tf.reduce_mean(h_start - h_end, 0))
        return [update_w, update_sb, update_hb]

    def reconstruction_error(self, dataset):
        err = tf.stop_gradient(dataset - self.gibbs_shs(dataset)[1])
        return tf.reduce_sum(err * err)

    def train(self,):
