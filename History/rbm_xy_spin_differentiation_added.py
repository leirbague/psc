import numpy as np
import numpy.random as rng
import theano
import theano.tensor as T
from qutip import *
from theano import function
from theano import shared
from matplotlib import pyplot as plt

def S(n, s):  # takes a binary
    try:
        res = np.asarray(list(format(s, '0%db' % n)), dtype='float') - 0.5
        return res
    except:
        return S(n,int(s, 2))

#Spin XY model hamiltonian generator
def H_simple(N):
    sx = sigmax()
    sy = sigmay()
    ide = qeye(2)
    H = 0
    for i in range(N-1):
        H += tensor([ide]*(N - 2 - i) + [sx,sx] + [ide]*(i))
        H += tensor([ide]*(N - 2 - i) + [sy,sy] + [ide]*(i))
    if N > 2:
        H += tensor([sx] + [ide] * (N - 2) + [sx])
        H += tensor([sy] + [ide] * (N - 2) + [sy]) 
    return H.data.toarray()

#size of visible and hidden layers
n_visible = 3
n_hidden = 9

#compute all spin configurations: [000],[001],[010],[011] etc..
hilbert = np.array(list(S(n_visible, i) for i in range(2 ** n_visible)))

#generate the hamiltonian of interest
hamilton = H_simple(n_visible)
hamilton = np.real(hamilton)

#Faire n'importe quoi avec l'hamiltonian pour tester
#hamilton[1][3] = 1
#hamilton[3][1] = 1
#hamilton[4][0] = hamilton[0][4] = -3

#visible layer
v = T.dvector('v')

##set of (complex) parameters that characterises the wavefunction
#w = shared(rng.rand(n_hidden, n_visible) + 1j * rng.rand(n_hidden, n_visible), name = 'w')
#a = shared(rng.rand(n_visible) + 1j * rng.rand(n_visible), name = 'a')
#b = shared(rng.rand(n_hidden) + 1j * rng.rand(n_hidden), name = 'b')
#params = [w, a, b]
##print(w.get_value())

#Alternative: use pure real parameters
w = shared(rng.rand(n_hidden, n_visible)  - 0.5, name = 'w')
a = shared(rng.rand(n_visible) - 0.5, name = 'a')
b = shared(rng.rand(n_hidden) - 0.5, name = 'b')
params = [w, a, b]

#wavefunction amplitude at [v]
psi = T.exp(T.dot(a, v)) * T.prod(2 * np.cosh(b + T.dot(w,v)))
wavefunction = function([v], psi)


#########################################
temp_sum = []
for s1 in range(2 ** n_visible):
    spin_config1 = map(int, list(str(bin(s1))[2:]))
    spin1 = np.array([0]*(n_visible - len(spin_config1)) + spin_config1) -0.5
    for s2 in range(s1, 2 ** n_visible):
        if hamilton[s1][s2] < 1.0:
            continue
        spin_config2 = map(int, list(str(bin(s2))[2:]))
#        print("spin configuration: ")        
        spin2 = np.array([0]*(n_visible - len(spin_config2)) + spin_config2) -0.5
#        print(spin1)
#        print(spin2)
#        print(hamilton[s1][s2])
        temp_sum.append( T.exp(T.dot(a, spin2)) * T.prod(2 * np.cosh(b + T.dot(w,spin2))) * hamilton[s1][s2] * T.exp(T.dot(a, spin1)) * T.prod(2 * np.cosh(b + T.dot(w,spin1))))
#        print(theano.pp(temp_sum[-1]))
e_total = sum(temp_sum)        
#
##compute the amplitude of the wavefunction at each spin configuration
#amp = []
#for sig in hilbert:
#    print(sig)
#    amp.append(wavefunction(sig))
#amp = np.array(amp)
#
##print(theano.pp(tt))
############################################


##construct energy function
#energ = T.dot(T.dot(amp.conj(), hamilton), amp)
##energ_at_v = psi
energy = function([], e_total)

#########################################################################
#derivative of energy
gw, ga, gb = T.grad(e_total, [w, a, b])
#d_E = function([], [gw, ga, gb])
#print(d_E())

#training
slope = 10 ** (-n_hidden - 1)

#print(slope)
train = function(
          [],
          outputs= [e_total, ga, gb],
          updates=((w, w - slope * gw), (a, a - slope * ga), (b, b - slope * gb)))

print("Initial (normalized) ground state wavefunction found using simple gradient descend:")
state = np.array(list(wavefunction(i) for i in hilbert))
norm = sum(state ** 2) ** (-0.5)
#print(norm)
#print(state)
state *= norm
print(state)

plt.xlabel('spin configuration: S')
plt.ylabel('ampitude (Psi(S))')
plt.scatter(range(len(state)), state, color="blue", linewidth=1.0)
plt.show()

criterion_of_stopping = 1e-8
e1 = 1
while True:
#    print("current iteration:")
    energyTotal, da, db = train()
    progress = abs(e1 - energyTotal) / e1
    if progress < criterion_of_stopping:
        break
    e1 = energyTotal
#    print(a.get_value())
#    print(da)
#    print("energy:")
#    print(energyTotal)
    






#########################################################################

##differentiate psi at a specific spin configuration
#gw, ga, gb = T.grad(psi, [w, a, b])
#d_psi = function([v], [gw, ga, gb])
#print("derivative done:")
#print(d_psi([0.5,-0.5,0.5]))
#
#O_k = [gw/psi, ga/psi, gb/psi]
#vd = function([v], O_k)
#print("vational derivative: ")
#print(vd([0.5,-0.5,-0.5]))

    


print("energy final:")
print(energy())

print("Final (normalized) ground state wavefunction found using simple gradient descend:")
state = np.array(list(wavefunction(i) for i in hilbert))
norm = sum(state ** 2) ** (-0.5)
#print(norm)
#print(state)
state *= norm
print(state)

plt.xlabel('spin configuration: S')
plt.ylabel('ampitude (Psi(S))')
plt.scatter(range(len(state)), state, color="blue", linewidth=1.0)
plt.show()
#print(np.array([1, 2, 3, 4]) ** 2)
#for i in hilbert:    
#    print(i)
#    print(wavefunction(i))
#    print()
    
############################################
#gw = T.grad(tt, w)
#grad = function([], gw)
#print(grad())
############################################

##
#energy2 = function([], energ)
#

############################################
#
#y = T.dvector('y')
#x = shared(np.array([[1,1,1],[1,2,3],[3,6,1.2]]), name = 'x')
#z = T.dot(T.exp(T.dot(x, y)),[0.1,0.1,0.1])
#
#f = function([y], z)
#print (f([1,1,2]))

#gx = T.grad(temp_sum, 12)
##fdx = function([v], gx)
#print(gx)
############################################



#finite difference method.
############################################

#ga = T.grad(energ, a)

##random walk
#pace = 1.0
#w_step = pace * (rng.rand(n_hidden, n_visible) - 0.5)
#a_step = pace * (rng.rand(n_visible) - 0.5)
#b_step = pace * (rng.rand(n_hidden) - 0.5)
##print(w_step)
############################################



############################################
#debug and testing
#print("for testing: ")
#test = 5
#print("w:")
#print(w.get_value())
#print("a:")
#print(a.get_value())
#print("b:")
#print(b.get_value())
#print(hilbert[test])
#print(wavefunction(hilbert[test]))
#print("energy by np:")
#print(energy2())
#print("testing ended")

#test_w = np.array(w.get_value())
#test_a = np.array(a.get_value())
#test_b = np.array(b.get_value())
##print(test_w)
##print(test_a)
#
##hilbert = np.array([[0, 0],[0,1],[1,0],[1,1]])
##test_w
#
#res = []
#for sigma in hilbert:
#    sigma = np.array(sigma)
#    res.append(np.exp(np.dot(test_a, sigma)) * np.prod(2 * np.cosh(test_b + np.dot(test_w, sigma))))
#    
#res = np.array(res)
#ener = np.dot(np.dot(res.conj(),hamilton), res)
#print(ener)
#
#def gradient(func = None):
#    func()
#    
#    return func()
#
#print(gradient(energy))

