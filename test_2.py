import tensorflow as tf
import numpy as np
import numpy.random as rng

#to a binary array representation of the quantum state
def to_bin(n, s):
    return [((s >> i) & 1) * 2 - 1 for i in reversed(range(n))]

def target_function(s):
    return np.float32(np.sin(s * 1.0 / 10))

# my_graph = tf.Graph()

N, M = 3, 4

batch_size = 10

default_sigma = np.array([1.0 * target_function(i) for i in range(2*N)],dtype=np.float32).reshape((N,2))
# print(default_sigma)

# a0 = rng.randn(N).astype(np.float32)#*np.exp(rng.rand(N)*np.pi*2*1j)
# b0 = rng.randn(M).astype(np.float32)#*np.exp(rng.rand(M)*np.pi*2*1j)
# w0 = rng.randn(M, N).astype(np.float32)#*np.exp(rng.rand(M, N)*np.pi*2*1j)

a0 = np.zeros(N, dtype=np.float32)
b0 = np.zeros(M, dtype=np.float32)
w0 = np.ones((M, N), dtype=np.float32)/10
#
# print(a0,b0)
# print(w0)

a = tf.transpose(tf.Variable(a0, tf.float32, name='a'))
b = tf.Variable(b0, tf.float32, name='b')
W = tf.Variable(w0, tf.float32, name='W')
sigma = tf.placeholder_with_default(default_sigma, shape=(N,2), name='Sigma')
target = tf.placeholder_with_default([18.0], shape=1, name='Target')


s1 = tf.matmul(W, sigma)
ss = tf.pack((b,b))
# theta = tf.add(tf.pack(b,b),s1)

print('OK')
# psi = tf.exp(tf.reduce_sum(tf.mul(a, sigma))) * tf.reduce_prod(tf.exp(theta) + tf.exp(-theta))
# #trial = tf.reduce_prod(tf.exp(tf.exp(-theta) + tf.exp(theta)))
#
# loss = tf.square(target - psi)
#
# optimizer = tf.train.GradientDescentOptimizer(0.00001)
# train = optimizer.minimize(loss)

init = tf.global_variables_initializer()
sess=tf.Session()
sess.run(init)

print(sess.run(s1))
print(sess.run(s2))

print(to_bin(10,3))
# print("before")
# print(sess.run(loss))
# print(sess.run(a), sess.run(b), sess.run(W))

# for i in range(1000):
#   sess.run(train)
#
# print("after")
# print(sess.run(loss))
# print(sess.run(a), sess.run(b), sess.run(W))


# Test results

# t = 2 * np.cosh(b0 + np.dot(w0, default_sigma))
# wf = np.exp(np.dot(default_sigma, a0)) * np.prod(t)
# print(wf)

# print(sess.run(-trial))