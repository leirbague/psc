'''
using variantional monte carlo (VMC) methode to attempt a faster solution of the ground state of Spin XY - problem
under construction...
'''

from RBM_with_metropolis import RBM
import time
import numpy as np
import matplotlib.pyplot as plt

def revert_binary(bitlist):
    out = 0
    bitlist = list(((int(i) + 1)/2) for i in bitlist)
    for bit in bitlist:
        out = (out << 1) | bit
    return out

def S(s, n):
    out = []
    for i in range(n):
        num = s >> i
        digit = (num & 1) * 2 - 1
        out = [digit] + out
    return out

#normalise a list
def normalise(l):
    return [float(i) / sum(l) for i in l]

def generate_params(softener, n, h):
    a = np.zeros(n)
    b = np.zeros(h)
    w = (np.random.normal(0, softener ** (-1), (h, n)))

    return a, b ,w

def diff(softener, n, h):
    a, b, w = generate_params(softener, n, h)
    # print("mean square a = {:.2f}, mean square b = {:.2f}, mean square w = {:.2f}".format(np.mean(a*a), np.mean(b*b), np.mean(w*w)))
    rbm = RBM(a, b, w)
    rbm.init_metropolis()
    lowest = 0
    highest = 0
    for i in range(2 ** n):
        value = rbm.ln_psi(S(i, n))
        if lowest > value:
            lowest = value
        if highest < value:
            highest = value
    return highest - lowest



# the amount of computational time is reasonable for n < 14
n = int(input('Input number of particles:'))

# recommended: h / n <= 4
h = int(input('Input number of hidden variables:'))


softener = 1

timer = time.time()

check = input('Auto-optimise parameters? [y/n]')

if check.lower() == 'y':
    print("Finding suitable initial parameters for RBM...")

    diff1 = diff(softener, n, h)
    diff2 = 0
    while np.abs(diff1 - diff2) > 5:
        softener += 2
        print('Trying parameters with softener = {}'.format(softener))
        diff2 = diff1
        diff1 = diff(softener, n, h)
        print('highest - lowest = {}'.format(diff1))
        if diff1 < 10:
            break
    print("Done, time taken: {}".format(time.time() - timer))


a, b, w = generate_params(softener, n, h)
rbm = RBM(a, b, w)
rbm.init_metropolis()
# print(rbm.w)
# print(rbm.a)
# print(rbm.b)
# print(rbm.integrator(10000))

ite = int(input('Input number of iterations:'))

timer = time.time()
print("Doing {} iterations of metropolis algorithm".format(ite))

z = [0] * (2 ** n)
x = range(2**n)

#a variable for tracking progress
percent = 0

for i in range(ite):
    z[rbm.walk()[0]] += 1
    # rbm.walk()
    if i % (ite / 10) == 0:
        percent += 10
        print("%{} of the iterations done".format(percent))

print("Done, time taken: {}".format(time.time() - timer))

y = []
y_ln = []
for i in x:
    y.append(rbm.psi(S(i, n)))
#     y_ln.append(rbm.ln_psi(S(n, i)))
# res = np.array([y, y_ln])

y = normalise([a*b for a,b in zip(y,y)])
z = normalise(z)
print('Average absolute error = {}'.format(sum(np.absolute(np.array(y) - np.array(z))) / 2 ** n))
# # print(res)


# fig = plt.figure()
# ax = fig.add_subplot(111)
plt.scatter(range(2 ** n),z, color = 'r')
plt.scatter(range(2 ** n),y, color = 'b')

# print(np.amax(np.array([z, y])) * 1.1

plt.ylim(0, np.amax(np.array([z, y])) * 1.1)
# plt.yscale('log')
plt.show()

m = 10000
timer = time.time()
e_sim = rbm.energy(m)[0]
print("time taken to calculate energy: {}".format(time.time() - timer))
#
# # timer = time.time()
# # e_sim_flip = rbm.energy_using_integrator(m)
# # print("time taken to calculate energy with integrator: {}".format(time.time() - timer)
#
#
# timer = time.time()
# e_exact = rbm.energy_exact()
# print("time taken to calculate exact energy: {}".format(time.time() - timer))
#
print("energy is: {}".format(e_sim))
# # print(rbm.theta_computation
# # print("OTOH, energy with integrator is {}".format(e_sim_flip)
# print("while exact value is {}".format(e_exact))
# print("percentage error of energy = {}".format(abs(e_sim - e_exact) / e_exact))
#
#

'''
th = rbm.theta([-1,-1,1])
print("without theta, {}".format(rbm.ln_psi([-1,-1,1], theta=None)))
print("with theta, {}".format(rbm.ln_psi([-1,-1,1], th)))
'''
