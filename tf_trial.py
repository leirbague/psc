import tensorflow as tf
import numpy as np
import numpy.random as rng

def to_bin(n, s):
    return [((s >> i) & 1) * 2 - 1 for i in reversed(range(n))]

def target_wave_function(s):
    n = len(s)
    return np.float32(np.sin(np.sum(s, axis=0)*6.28/n) / 100**n)

def batch(n, func, batch_size):
    for s in range(0, 2 ** n, batch_size):
        sigma = np.array(np.transpose([to_bin(n,i) for i in range(s, min(s+batch_size, 2**n))]), dtype='f')
        psi = np.array(np.transpose([func(to_bin(n,i)) for i in range(s, min(s+batch_size, 2**n))]), dtype='f')
        yield sigma, psi

# for testing
def all_in_one_rbm_calculator(a, b, w, s):
    t = 2 * np.cosh(b + np.dot(w, s))
    psi = np.exp(np.dot(np.transpose(a), s)) * np.prod(t,axis=0)
    return psi[0]

N, M = 5, 5
batch_size = 4
rate = 1e-8
softener = 100000
steps = 100000
# logs_path = '/Users/Yunzhe/Projects/PSC/Codes/summary'

a0 = np.array(rng.randn(N,1)/softener,dtype=np.float32)
b0 = np.array(rng.randn(M,1)/softener,dtype=np.float32)
w0 = np.array(rng.randn(M,N) / softener,dtype=np.float32)

with tf.name_scope('Inputs'):
    a = tf.Variable(a0, tf.float32, name='a')
    b = tf.Variable(b0, tf.float32, name='b')
    W = tf.Variable(w0, tf.float32, name='W')
    sigma = tf.placeholder(tf.float32, shape=(N, None), name='Sigma')
    psi_target = tf.placeholder(tf.float32, shape=None, name='Target')

with tf.name_scope('Model'):
    s1 = tf.matmul(W, sigma)
    theta = tf.add(b,s1)
    psi_output = tf.exp(tf.reduce_sum(tf.multiply(a, sigma),axis=0)) * tf.reduce_prod(tf.exp(theta) + tf.exp(-theta),axis=0)

with tf.name_scope('Loss'):
    loss = tf.nn.l2_loss(psi_target - psi_output)

with tf.name_scope('Optimiser'):
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=rate)
    train = optimizer.minimize(loss)

# Initialise the graph
init = tf.global_variables_initializer()

# tf.summary.scalar('Loss', loss)
# merged_summary_op = tf.summary.merge_all()

with tf.Session() as sess:
    sess.run(init)

    # op to write logs to Tensorboard
    # summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    # display selected parameters

    # print(input_sigma)
    # print(sess.run(tf.mul(a, sigma)))
    # print(sess.run(tf.exp(tf.reduce_sum(tf.mul(a, sigma),axis=0))))
    # print(sess.run(tf.reduce_prod(tf.exp(theta) + tf.exp(-theta),axis=0)))
    # print(sess.run(s1))
    # print(sess.run(b))
    # print(sess.run(theta))
    # print(sess.run(psi_target),'\n')
    # print(sess.run(psi_output))
    # print(all_in_one_rbm_calculator(a0,b0,w0,input_sigma),'\n')
    # print(sess.run(loss))

    # training

    test_state = np.array(rng.randint(2, size=(N,10)) * 2.0 - 1,dtype='f')
    test_psi = np.array([target_wave_function(s) for s in np.transpose(test_state)],dtype='f')

    print("before")
    # print(test_psi)
    # print(sess.run(a), '\n', sess.run(b), '\n', sess.run(W), '\n')

    # print('loss: ', sess.run([psi_output, loss], feed_dict={sigma: test_state, psi_target: test_psi})[1])

    # training samples
    samples = batch(N, target_wave_function, batch_size=batch_size)

    # optimisation
    # for n in range(1000):
    #     for input, target in samples:
    #         sess.run(train, feed_dict={sigma: input, psi_target: target})
    for n in range(steps):
        test_state = np.array(rng.randint(2, size=(N, 1)) * 2.0 - 1, dtype='f')
        test_psi = np.array([target_wave_function(s) for s in np.transpose(test_state)], dtype='f')
        _, l = sess.run([train,loss], feed_dict={sigma: test_state, psi_target: test_psi})
        if (n - (steps / 10) * (n // (steps / 10))) == 0:
            print(l)

    # test_state = np.array(rng.randint(2, size=(N, 10)) * 2.0 - 1, dtype='f')
    # test_psi = np.array([target_wave_function(s) for s in np.transpose(test_state)], dtype='f')
    # print("after")
    # print(test_psi)
    # print(sess.run(a), sess.run(b), sess.run(W))
    # print('out: ', sess.run([psi_output, loss], feed_dict={sigma: test_state, psi_target: test_psi})[0])
    #




