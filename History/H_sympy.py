# -*- coding: utf-8 -*-

import qutip
import matplotlib.pyplot as plt
from sympy import *
from sympy.matrices import SparseMatrix
import scipy as sp
import numpy as np
from sympy.printing.theanocode import theano_function
import theano
init_printing()
def H_simple(N):
	ide = qutip.qeye(2)
	sx = qutip.sigmax()
	sy = qutip.sigmay()
	H = 0
	if N > 2:
		H += qutip.tensor([sx] + [ide]*(N-2) + [sx])
		H += qutip.tensor([sy] + [ide]*(N-2) + [sy])
	for i in range(N-1):
		H += qutip.tensor([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += qutip.tensor([ide]*i+[sy,sy]+[ide]*(N-i-2))
	return H
"""
#multiple kronecker matrix product
def multi_sc(S):
	H = 1
	for m in S:
		H = sp.sparse.kron(H, m)
	return H

def multi(S):
	H = 1
	for m in S:
		H = np.kron(H, m)
	return H

def H_np(N):
	ide = np.eye(2)
	sx = np.array([[0,1],[1,0]])
	sy = np.array([[0,-I],[I,0]])
	sz = np.array([[1,0],[0,-1]])

	H = 0
	if N > 2:
		H += multi([sx] + [ide]*(N-2) + [sx])
		H += multi([sy] + [ide]*(N-2) + [sy])
	for i in range(N-1):
		H += multi([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += multi([ide]*i+[sy,sy]+[ide]*(N-i-2))
	return H

def H_scipy(N):
	ide = sp.sparse.identity(2)
	sx = sp.sparse.csr_matrix([[0,1],[1,0]])
	sy = sp.sparse.csr_matrix([[0,-I],[I,0]], dtype = complex)
	sz = sp.sparse.csr_matrix([[1,0],[0,-1]])

	H = 0
	if N > 2:
		H += multi_sc([sx] + [ide]*(N-2) + [sx])
		H += multi_sc([sy] + [ide]*(N-2) + [sy])
	for i in range(N-1):
		H += multi_sc([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += multi_sc([ide]*i+[sy,sy]+[ide]*(N-i-2))
	return H
"""
import time

n = 5   # n < 17

# ticks = time.time()
print("Constructing the hamiltonian...")
Hamilton = H_simple(n).data
# print(time.time() - ticks)
# ticks = time.time()
#Hamilton2 = H_scipy(n)
#print(time.time() - ticks)




# # print("Constructing the theano function...")
# Psi = MatrixSymbol('Psi', 2**n, 1)
# # print((Psi.T * Matrix(Hamilton) * Psi).as_explicit())
# E = theano_function([Psi], [Psi.T * Matrix(Hamilton) * Psi])

# print(time.time() - ticks)
# ticks = time.time()

# def format(input):
# 	input = input.reshape(input.shape[0],1)
# 	return input

# print("Evaluating energy...")
# energy = E(format(np.array([1,2,3,4,4,1,2,3] * 4)))
# print(energy)


# print(time.time() - ticks)
# ticks = time.time()
