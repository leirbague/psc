# -*- coding: utf-8 -*-

import qutip
import matplotlib.pyplot as plt
# from sympy import *
import scipy.sparse as sps
import scipy as sp
import numpy as np
# from sympy.printing.theanocode import theano_function
# import theano
import itertools
# init_printing()
def H_simple(N):
	ide = qutip.qeye(2)
	sx = qutip.sigmax()
	sy = qutip.sigmay()
	sz = qutip.sigmaz()
	H = 0
	if N > 2:
		H += qutip.tensor([sx] + [ide]*(N-2) + [sx])
		H += qutip.tensor([sy] + [ide]*(N-2) + [sy])
		# H += qutip.tensor([sz] + [ide]*(N-2) + [sz])
	for i in range(N-1):
		H += qutip.tensor([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += qutip.tensor([ide]*i+[sy,sy]+[ide]*(N-i-2))
		# H += qutip.tensor([ide]*i+[sz,sz]+[ide]*(N-i-2))
	return H

#multiple kronecker matrix product
def multi_sc(S):
	H = 1
	for m in S:
		H = sp.sparse.kron(H, m)
	return H

def multi(S):
	H = 1
	for m in S:
		H = np.kron(H, m)
	return H

def H_np(N):
	ide = np.eye(2)
	sx = np.array([[0,1],[1,0]])
	sy = np.array([[0,-I],[I,0]])
	sz = np.array([[1,0],[0,-1]])

	H = 0
	if N > 2:
		H += multi([sx] + [ide]*(N-2) + [sx])
		H += multi([sy] + [ide]*(N-2) + [sy])
	for i in range(N-1):
		H += multi([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += multi([ide]*i+[sy,sy]+[ide]*(N-i-2))
	return H

def H_scipy(N):
	ide = sp.sparse.identity(2)
	sx = sp.sparse.csr_matrix([[0,1],[1,0]])
	sy = sp.sparse.csr_matrix([[0,-I],[I,0]], dtype = complex)
	sz = sp.sparse.csr_matrix([[1,0],[0,-1]])

	H = 0
	if N > 2:
		H += multi_sc([sx] + [ide]*(N-2) + [sx])
		H += multi_sc([sy] + [ide]*(N-2) + [sy])
	for i in range(N-1):
		H += multi_sc([ide]*(N-i-2)+[sx,sx]+[ide]*i)
		H += multi_sc([ide]*i+[sy,sy]+[ide]*(N-i-2))
	return H

def parcour_col(x):
    cx = x.tocoo()
    for j in cx.row:
        print j


#taken from stack exchange
def using_tocoo_izip(x):
    cx = x.tocoo()    
    for i,j,v in itertools.izip(cx.row, cx.col, cx.data):
        (i,j,v)

import time


n = 7

print("Constructing the hamiltonian...")
Hamilton = H_simple(n).data
# Hamilton_row = sps.coo_matrix(Hamilton)
# Hamilton2 = H_np(n)
# Hamilton3 = H_np(n)

# tick = time.time()
# # for i in range(100):
# parcour_col(Hamilton.getcol(100))
# print time.time() - tick

# tick = time.time()
# # for i in range(100):
# parcour_col(Hamilton_row.getcol(100))
# print time.time() - tick


# print(h - Hamilton).max()



plt.figure(1)
plt.spy(Hamilton)


# # plt.figure(3)
# # plt.spy(Hamilton3)

plt.show()


# tick = time.time()
# for i in range(1000):
# 	1 << 3
# print time.time() - tick

# tick = time.time()
# for i in range(1000):
# 	1 * (2**3)
# print time.time() - tick
