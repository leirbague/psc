
import numpy as np
# import numpy.random as rng

def to_bin(n, s):
    return [((s >> i) & 1) * 2 - 1 for i in reversed(range(n))]

def target_wave_function(s):
    n = len(s)
    return np.float32(np.sin(np.sum(s, axis=0)*6.28/n)/100)

def batch(n, func, batch_size):
    for s in range(0, 2 ** n, batch_size):
        sigma = np.array(np.transpose([to_bin(n,i) for i in range(s, min(s+batch_size, 2**n))]), dtype='f')
        psi = np.transpose([func(to_bin(n,i)) for i in range(s, min(s+batch_size, 2**n))])
        yield sigma, psi


# def batch_dispenser(iterable, batch_size=1):
#     l = len(iterable)
#     for ndx in range(0, l, batch_size):
#         yield iterable[ndx:min(ndx + batch_size, l)]

l = batch(10, target_wave_function, 3)
i, o = next(l, None)
for i, j in l:
    print(j)
# print(next(l, None))
# print(next(l, None))