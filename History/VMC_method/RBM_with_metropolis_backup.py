'''
using variantional monte carlo (VMC) methode to attempt a faster solution of the ground state of Spin XY - problem
under construction...
'''

import numpy as np

def revert_binary(bitlist):
    output = 0
    bitlist = list(((int(i) + 1)/2) for i in bitlist)
    for bit in bitlist:
        output = (output << 1) | bit
    return output

class RBM:
    'Restricted boltzmann machine with metropolis algorithm incorporated'

    def to_binary(self, s):
        return [((s >> i) & 1) * 2 - 1 for i in reversed(range(self.n))]

    # Hamiltonian of spin xy model. size = number of particles
    def row(self, index):
        mask = 3  # binary = 11
        for i in range(self.n - 1):
            if (bool(((index >> i) ^ (index >> i + 1)) & 1)):  # check if the ith degit is different from (i + 1)th digits
                yield index ^ (mask << i)  # flip digits at i and i + 1
        if (bool((index >> (self.n - 1)) ^ (index & 1))):  # check the first against the last digit
            yield index ^ (1 | (1 << (self.n - 1)))

    #effective angle
    def theta(self, basis):
        if isinstance(basis, int):
            basis = self.to_binary(basis)
        return self.b + np.dot(self.w, basis)

    #natural log of the value of the wave function at configuration 'basis'
    def ln_psi(self, basis, theta = None):
        if isinstance(basis, int):
            basis = self.to_binary(basis)
        m = self.b.shape[0]
        ln_2 = np.log(2)
        if theta is None:
            t = self.theta(basis)
        else:
            t = theta
        return np.dot(self.a, basis) + m * ln_2 + np.sum(np.log(np.cosh(t)))

    #the value of the wave function at configuration 'basis'
    def psi(self, basis, theta = None):
        return np.exp(self.ln_psi(basis, theta))

    #indices counting from the right
    def flip(self, i):
        theta = self.current_effective_angle
        new_basis = self.current_basis
        mask = 1 << i
        new_basis = new_basis ^ mask
        digit = int(bool(new_basis & mask)) * 2 - 1
        theta = theta + 2 * self.w[:, self.n - (i + 1)] * digit
        psi = self.ln_psi(new_basis, theta)
        return new_basis, theta, psi

    #take a random step
    def walk(self):
        index_to_flip = np.random.randint(self.n)
        new_basis, newTheta, changed_ln_psi = self.flip(index_to_flip)
        dice = np.random.ranf()
        if ((changed_ln_psi > self.current_ln_psi) or (dice < np.exp(2 * (changed_ln_psi - self.current_ln_psi)))):
            self.current_effective_angle, self.current_basis, self.current_ln_psi = newTheta, new_basis, changed_ln_psi
            return self.current_basis, self.current_ln_psi, self.current_effective_angle
        else:
            return self.current_basis, self.current_ln_psi, self.current_effective_angle

    #initialise the metropolis algorithms.
    def init_metropolis(self, initial_steps = 10):
        self.current_basis = np.random.randint(2 ** self.n)  # an integer
        self.current_effective_angle = self.theta(self.current_basis)
        self.current_ln_psi = self.ln_psi(self.current_basis, self.current_effective_angle)
        for i in range(initial_steps):
            self.walk()

    def loc_energy(self, walker_status):
        basis, ln_psi, theta = walker_status
        return 2 * sum([self.psi(j) for j in self.row(basis)]) / np.exp(ln_psi)

    def energy(self, steps = 1000):
        # return self.integrator(self.loc_energy, steps = 10000)
        accumulate = 0
        for i in range(steps):
            basis, ln_psi, theta = self.walk()
            psi = np.exp(ln_psi)
            temp = 0  #local energy at "basis"
            for j in self.row(basis):
                temp += self.psi(j)
            accumulate += (temp / psi)
        return 2 * accumulate / steps

    def energy_using_integrator(self, steps = 1000):
        return self.integrator(self.loc_energy, steps=steps)

    def integrator(self, functions, steps = 1000):
        if (not isinstance(functions, list)):
            functions = [functions]
        sums = np.array([0] * len(functions), dtype=np.float64)
        for i in range(steps):
            walker_status = self.walk()
            sums += np.array([f(walker_status) for f in functions])
        # print "sum is {}".format(sums[0])
        return sums / steps



    def normaliser(self):
        norm = 0
        for i in range(2 ** self.n):
            norm += self.psi(i)**2
        return norm ** (0.5)

    def energy_exact(self):
        accumulate = 0
        for i in range(2 ** self.n):
            for j in self.row(i):
                accumulate += self.psi(i) * 2 * self.psi(j)
        return accumulate / (self.normaliser() ** 2)

    def __init__(self, a, b, w, n):
        self.n = n
        self.a = a
        self.b = b
        self.w = w


