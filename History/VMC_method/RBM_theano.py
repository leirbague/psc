'''
using variantional monte carlo (VMC) methode to attempt a faster solution of the ground state of Spin XY - problem
under construction...
I added theano to speed up computation...
'''

import numpy as np
import theano
import theano.tensor as T
from theano import shared
# from theano.ifelse import ifelse
# from theano.compile.io import In


class RBM:
    'Restricted boltzmann machine with metropolis algorithm incorporated'

    @staticmethod
    def generate_params(n, h, softener = 1):
        a = shared(np.zeros(n), name = 'a')
        b = shared(np.zeros(h), name = 'b')
        w = shared(np.random.normal(0, softener ** (-1), (h, n)), name = 'w')
        return a, b ,w

    def __init__(self, n, h):
        self.n = n
        self.h = h
        softener = 20
        self.a, self.b, self.w = RBM.generate_params(n, h, softener)

        state = T.vector('state')
        given_theta = T.vector('given_theta')
        th = self.b + T.dot(self.w, state)
        ln = T.dot(self.a, state) + h * np.log(2) + T.sum(T.log(T.cosh(th)))
        ln_from_theta = T.dot(self.a, state) + h * np.log(2) + T.sum(T.log(T.cosh(given_theta)))
        ps = T.exp(ln)

        self.theta = theano.function(inputs = [state], outputs = th)
        self.ln_psi = theano.function(inputs = [state], outputs = ln)
        self.ln_psi_from_theta = theano.function(inputs = [state, given_theta], outputs = ln_from_theta)
        self.psi = theano.function(inputs = [state], outputs = ps)
    
    #effective angle
    def compute_theta(self, state):
        return  self.theta(state)

    #natural log of the value of the wave function at configuration 'state'
    def compute_ln_psi(self, state, theta = None):
        if theta is None:
            return self.ln_psi(state)
        else:
            print "bypassed"
            return self.ln_psi_from_theta(state, theta)

    #the value of the wave function at configuration 'state'
    def compute_psi(self, state, theta = None):
        return np.exp(self.compute_ln_psi(state, theta))

    #take a random step
    def walk(self):
        #flip the value of spin of a random particle
        flip_index = np.random.randint(self.n)
        newState = list(self.state)
        newState[flip_index] *= -1
        #update the lookup table for theta values
        newTheta = self.oldTheta + 2 * self.w.get_value()[:,flip_index] * newState[flip_index]
        test_theta = self.compute_theta(newState)
        #if not (newTheta == test_theta).all():
        #    print(newTheta)
        #    print(test_theta)
        psi2 = self.compute_ln_psi(newState, newTheta)
        
        # testing
        # print('psi2 = {:.2f}, psi1 = {:.2f}'.format(psi2, self.psi1))
        # print('state 2 = {}, state 1 = {}'.format(newState, self.state))
        #determine if accept the new state. Probability = min(1, (psi_new / psi_old)^2)
        dice = np.random.ranf()
        ratio = np.exp(2 * (psi2 - self.psi1))
        if ((psi2 > self.psi1) or (dice < np.exp(2 * (psi2 - self.psi1)))):
        #     if psi2 > self.psi1:
        #         print('accept with psi2 > psi1')
        #     else:
        #         print('accept with psi2 < psi1, ratio = {:.2f}, dice = {:.2f}'.format(np.exp(2 * (psi2 - self.psi1)), dice))
            self.oldTheta = newTheta
            self.state = newState
            self.psi1 = psi2
            return self.state
        else:
            # if psi2 < self.psi1:
            #     print('reject, ratio = {:.2f}, dice = {:.2f}'.format(np.exp(2 * (psi2 - self.psi1)), dice))
            return self.state

    #initialise the metropolis algorithms.
    def init_metropolis(self, initial_steps = 10):
        self.state = np.random.randint(2, size = self.n) * 2 - 1
        # lookup table of theta values for faster computation
        self.oldTheta = self.compute_theta(self.state)
        self.psi1 = self.compute_ln_psi(self.state, self.oldTheta)
        for i in range(initial_steps):
            self.walk()


# #convert a decimal number s into a binary with n digits
# def S(n, s):  # takes a binary
#     try:
#         res = np.asarray(list(format(s, '0%db' % n)), dtype='int') * 2 - 1
#         return res
#     except:
#         return S(n,int(s, 2))

import time
rbm = RBM(10,40)




rbm.init_metropolis()
print "start..."
tick = time.time()
for i in range(10000):
    rbm.walk()

print time.time() - tick

