#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 20 21:16:11 2016

@author: gabriel
"""

import numpy as np
import theano
import theano.tensor as T
import math
from H import H_simple

# debugging parameters
theano.config.exception_verbosity = 'high'
float = theano.config.floatX

def rand_cmatrix(n,m,std): # n x m matrix with random entries of imaginary and real part normally distribuited
    return np.random.normal(scale = std,size = (n,m)) + 1j*np.random.normal(scale = std,size = (n,m))

def rand_matrix(n,m,std): # n x m matrix with random entries of imaginary and real part normally distribuited
    return np.random.normal(scale = std,size = (n,m))


class RBM(object):
    """Restricted Boltzmann Machine (RBM)  """
    def __init__(
        self,
        n=10, # number of visible layers
        m=40, # number of hidden layers
        mode = 'real',
        input=None,
        W=None,
        b=None,
        a=None,
    ):
        self.n = n
        self.m = m

        if W is None: # weights' matrix W_ij
            std = 1.0/math.sqrt(n*m + m + n)
            W = theano.shared(value=rand_matrix(m, n, std), name='W', borrow=True)

        if b is None: # vector {b_j} of hidden layer biases
            b = theano.shared(value=np.zeros(m,dtype=theano.config.floatX),name='b',borrow=True)

        if a is None: # vector {a_i} of spin biases
            a = theano.shared(value=np.zeros(n,dtype=theano.config.floatX),name='a',borrow=True)

        # initialize input layer for standalone RBM or deeper layer
        self.input = input
        if not input:
            self.input = T.dvector('input')

        self.W = W
        self.b = b
        self.a = a
        self.params = [self.W, self.b, self.a] # shared parameters

    def psi_amphase(self,s):
        spin_factor = T.exp(T.dot(self.a, s))
        hidden_factor = T.prod(2 * np.cosh(self.b + T.dot(self.W, s)))
        return spin_factor * hidden_factor

    def psi_function(self):
        # compute all spin configurations: [---],[--+],[-+-],[-++] etc..
        hilbert = np.asarray([self.S(i) for i in range(2 ** self.n)])

    def S(self, s):  # takes a binary
        try:
            res = .5 - np.asarray(list(format(s, '0%db' % self.n)), dtype = float)
            return res
        except:
            return self.S(int(s, 2))

    def total_energy_fast(self,H): # TODO: FIND another way of representing spins; look at sympy library
        non = list(zip(*H.nonzero()))
        temp = []
        for ij in non:
            s1, s2 = ij
            temp.append(self.psi_amphase(self.S(s2)) * np.real(H[s1,s2]) * self.psi_amphase(self.S(s1)))
        return T.sum(temp)

    def total_energy(self, H):
        temp_sum =[]
        for s1 in range(2 ** n):
            spin1 = self.S(s1)
            for s2 in range(s1, 2 ** n):
                h_ij = np.real(H[s1,s2])
                if abs(h_ij)==0: continue
                spin2 = self.S(s2)
                psi_spin2 = T.exp(T.dot(self.a, spin2)) * T.prod(2 * np.cosh(self.b + T.dot(self.W, spin2)))
                psi_spin1 = T.exp(T.dot(self.a, spin1)) * T.prod(2 * np.cosh(self.b + T.dot(self.W, spin1)))
                temp_sum.append(psi_spin1 * np.real(H[s1,s2]) * psi_spin2)
        return T.sum(temp_sum)

    def grad_energy_total(self,H):
        energy = self.total_energy(H)
        gw, ga, gb = T.grad(energy, [self.W, self.a, self.b])
        return gw, ga, gb

if __name__ == '__main__':
    n=4
    m=16
    rbm = RBM(n,m)
    H = H_simple(n).data
    print(rbm.total_energy(H).eval())
    print(rbm.total_energy_fast(H).eval())