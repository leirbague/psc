
# size = number of particles
def row(index, size):
	mask = 3 # binary = 11
	non_zeros = []
	for i in range(size - 1):
		if (bool(((index >> i) ^ (index >> i + 1)) & 1)):     #check if the ith degit is different from jth digits
			non_zeros.append(index ^ (mask << i))             # flip digits at i and i + 1
	if (bool((index >> (size - 1)) ^ (index & 1))):           # check the first against the last digit
		non_zeros.append(index ^ (1 | (1 << (size - 1))))
	return non_zeros


#testing

'''
n = 7
h = np.zeros((2**n, 2 **n))
for i in range(2**n):
	r = row(i, n)
	for j in r:
		h[i,j] = 2


plt.figure(1)
plt.spy(h)

plt.show()
'''