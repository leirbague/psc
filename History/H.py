# -*- coding: utf-8 -*-
"""
Created on Fri Sep 23 19:32:19 2016

@author: gabriel
"""

import qutip


def H_simple(N,ending=False):
    
    ide = qutip.qeye(2)
    sx = qutip.sigmax()/2
    sy = qutip.sigmay()/2

    if(ending):
        H = qutip.tensor([sx]+[ide]*(N-2)+[sx])
        if(N==2): return H.eigenenergies()
    else: H = 0
    
    for i in range(N-1):
        H += qutip.tensor([ide]*(N-i-2)+[sx,sx]+[ide]*i)
        H += qutip.tensor([ide]*i+[sy,sy]+[ide]*(N-i-2))
    return H
        

from scipy.sparse import dok_matrix
from scipy.sparse.linalg import eigsh

def bit_swap(s, i, j):
      s = list(s)
      s[i], s[j] = s[j], s[i]
      return ''.join(s)

def H_sparse(n,ending=False,debug=False,datab=False):
    S = dok_matrix((2**n, 2**n), dtype=float)
    
    for k in range(2**n):
        i = format(k,'0{0}b'.format(n)) # if n=3, k=w => i = '010'
    
        if(ending):
            j = bit_swap(i,0,-1)  
            if(i!=j): S[int(i,2),int(j,2)] = 2
        
        for l in range(n-1):
            j= bit_swap(i,l,l+1)
            if(i!=j):
                S[int(i,2),int(j,2)] = 2
    k = 2**n - 1
    
    
    vals, vecs = eigsh(S,k,which='LM')
    
    if(datab):
        vecs = dok_to_dict(dok_matrix(np.round(vecs,14)).transpose())
        vals = np.round(vals,14)
        S = dok_to_dict(S)
        return vals,vecs,S
    return vals
    

import json
import numpy as np

def update_database(n,data):
    jsonFile = open("H_database.json", "r")
    all_data = json.load(jsonFile)
    jsonFile.close()
    
    all_data[n] = data    
    
    jsonFile = open("H_database.json", "w")
    jsonFile.write(json.dumps(all_data,default=float))
    jsonFile.close()


def jsonable_data(n):
    vals,vecs,H = H_sparse(n,datab=True)
    return {"values": list(vals), "vectors": vecs, "H": H}

def first_data():
    start_data = {}
    
    start_data[2] = jsonable_data(2)
    
    jsonFile = open("H_database.json", "w")
    jsonFile.write(json.dumps(start_data,default=float))
    jsonFile.close()

def get_all_data():
    jsonFile = open("H_database.json", "r")
    all_data = json.load(jsonFile)
    jsonFile.close()
    return all_data

def generate_data_until(N):
    n = len(get_all_data())+2    
    while(n<=N):
        data = jsonable_data(n)
        update_database(n,data)
        print("Successfully saved data for n = {0}".format(n))
        n+=1

def dok_to_dict(S):
    non = S.nonzero()
    dic = {}
    for i in zip(*non):
        dic["%d,%d" % i] = S[i]
    return dic

def dict_to_dok(dic,N):
    S = dok_matrix((2**N,2**N-1),dtype=float)   
    for i in dic:
        S[tuple(map(int, i.split(',')))] = dic[i]
    return S
#import numpy as np
# debug result: very small deviation with H_sparse from H_simple
#for i in range(3,11):
#    e = np.sort(H_simple(i)) - np.sort(np.append(H_sparse(i),0))
#    print(np.dot(e,e)**(.5))