import numpy as np
import numpy.random as rng
import theano
import theano.tensor as T
from qutip import *

#rng.seed(2)

#Spin XY model hamiltonian generator
def H_simple(N):
    sx = sigmax()
    sy = sigmay()
    ide = qeye(2)
    H = 0
    for i in range(N-1):
        H += tensor([ide]*(N - 2 - i) + [sx,sx] + [ide]*(i))
        H += tensor([ide]*(N - 2 - i) + [sy,sy] + [ide]*(i))
    if N > 2:
        H += tensor([sx] + [ide] * (N - 2) + [sx])
        H += tensor([sy] + [ide] * (N - 2) + [sy]) 
    return H.data.toarray()

#size of visible and hidden layers
n_visible = 3
n_hidden = 10



#generate the hamiltonian of interest
hamilton = H_simple(n_visible)

#visible layer
v = T.dvector('v')

#set of (complex) parameters that characterises the wavefunction
#w = theano.shared(rng.rand(n_hidden, n_visible) + 1j * rng.rand(n_hidden, n_visible), name = 'w')
#a = theano.shared(rng.rand(n_visible) + 1j * rng.rand(n_visible), name = 'a')
#b = theano.shared(rng.rand(n_hidden) + 1j * rng.rand(n_hidden), name = 'b')
#params = [w, a, b]
#print(w.get_value())

w = theano.shared(rng.rand(n_hidden, n_visible), name = 'w')
a = theano.shared(rng.rand(n_visible) , name = 'a')
b = theano.shared(rng.rand(n_hidden) , name = 'b')
params = [w, a, b]


#wavefunction amplitude at [v]
psi = T.exp(T.dot(a, v)) * T.prod(2 * np.cosh(b + T.dot(w,v)))

wavefunction = theano.function([v], psi)

#compute the amplitude of the wavefunction at each spin configuration
amp = []
for i in hilbert:
    amp.append(wavefunction(i))
amp = np.array(amp)

#construct energy function
energ = T.dot(T.dot(amp.conj(), hamilton), amp)

energy = theano.function([], energ)

#compute gradient
gw, ga, gb = T.grad(energ, [w, a, b])



#debug and testing
print("for testing: ")
test = 5
print("w:")
print(w.get_value())
print("a:")
print(a.get_value())
print("b:")
print(b.get_value())
print(hilbert[test])
print(wavefunction(hilbert[test]))
print(energy())

#test_w = np.array(w.get_value())
#test_a = np.array(a.get_value())
#test_b = np.array(b.get_value())
##print(test_w)
##print(test_a)
#
##hilbert = np.array([[0, 0],[0,1],[1,0],[1,1]])
##test_w
#
#res = []
#for sigma in hilbert:
#    sigma = np.array(sigma)
#    res.append(np.exp(np.dot(test_a, sigma)) * np.prod(2 * np.cosh(test_b + np.dot(test_w, sigma))))
#    
#res = np.array(res)
#ener = np.dot(np.dot(res.conj(),hamilton), res)
#print(ener)



