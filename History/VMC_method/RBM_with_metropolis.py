'''
using variantional monte carlo (VMC) methode to attempt a faster solution of the ground state of Spin XY - problem
under construction...
'''

import numpy as np
import math

def revert_binary(bitlist):
    output = 0
    for bit in bitlist:
        output = (output << 1) | ((bit + 1) / 2)
    return output

class RBM:
    'Restricted boltzmann machine with metropolis algorithm incorporated'

    def to_binary(self, s):
        return [((s >> i) & 1) * 2 - 1 for i in reversed(range(self.n))]

    # Hamiltonian of spin xy model. size = number of particles
    def row(self, index):
        mask = 3  # binary = 11
        for i in range(self.n - 1):
            if (bool(((index >> i) ^ (index >> i + 1)) & 1)):  # check if the ith degit is different from (i + 1)th digits
                yield index ^ (mask << i)  # flip digits at i and i + 1
        if (bool((index >> (self.n - 1)) ^ (index & 1))):  # check the first against the last digit
            yield index ^ (1 | (1 << (self.n - 1)))

    #effective angle
    def theta(self, basis):
        if isinstance(basis, int):
            basis = self.to_binary(basis)
        return self.b + np.dot(self.w, basis)

    #natural log of the value of the wave function at configuration 'basis'
    def ln_psi(self, basis, theta = None):
        if isinstance(basis, int):
            basis = self.to_binary(basis)
        m = self.b.shape[0]
        ln_2 = np.log(2)
        if theta is None:
            t = self.theta(basis)
        else:
            t = theta
        return np.dot(self.a, basis) + m * ln_2 + np.sum(np.log(np.cosh(t)))

    #the value of the wave function at configuration 'basis'
    def psi(self, basis, theta = None):
        return np.exp(self.ln_psi(basis, theta))

    #indices counting from the right
    def flip(self, i):
        theta = self.current_effective_angle
        new_basis = self.current_basis
        mask = 1 << i
        new_basis = new_basis ^ mask
        digit = int(bool(new_basis & mask)) * 2 - 1
        theta = theta + 2 * self.w[:, self.n - (i + 1)] * digit
        psi = self.ln_psi(new_basis, theta)
        return new_basis, theta, psi

    #take a random step
    def walk(self):
        index_to_flip = np.random.randint(self.n)
        new_basis, newTheta, changed_ln_psi = self.flip(index_to_flip)
        dice = np.random.ranf()
        if ((changed_ln_psi > self.current_ln_psi) or (dice < np.exp(2 * (changed_ln_psi - self.current_ln_psi)))):
            self.current_effective_angle, self.current_basis, self.current_ln_psi = newTheta, new_basis, changed_ln_psi
            return self.current_basis, self.current_ln_psi, self.current_effective_angle
        else:
            return self.current_basis, self.current_ln_psi, self.current_effective_angle

    #initialise the metropolis algorithms.
    def init_metropolis(self, initial_steps = 10):
        self.current_basis = np.random.randint(2 ** self.n)  # an integer
        self.current_effective_angle = self.theta(self.current_basis)
        self.current_ln_psi = self.ln_psi(self.current_basis, self.current_effective_angle)
        for i in range(initial_steps):
            self.walk()

    def loc_energy(self, walker):
        basis, ln_psi, theta = walker
        return 2 * math.fsum([self.psi(j) for j in self.row(basis)]) / np.exp(ln_psi)

    def o_a(self, walker):
        return self.to_binary(walker[0])

    def o_b(self, walker):
        return np.tanh(walker[2])

    def o_w(self, walker):
        return np.outer(self.o_b(walker), self.o_a(walker)).flatten()

    def packer(self, walker):
        pack = np.concatenate([self.o_a(walker), self.o_b(walker), self.o_w(walker)])
        return pack

    def integrator(self, steps = 10000):
        return np.mean([self.packer(self.walk()) for i in range(steps)], axis=0)

    def normaliser(self):
        norm = 0
        for i in range(2 ** self.n):
            norm += self.psi(i)**2
        return norm ** (0.5)

    def energy_exact(self):
        accumulate = 0
        for i in range(2 ** self.n):
            for j in self.row(i):
                accumulate += self.psi(i) * 2 * self.psi(j)
        return accumulate / (self.normaliser() ** 2)

    def __init__(self, a, b, w):
        self.n = a.shape[0]
        self.h = b.shape[0]
        self.a = a
        self.b = b
        self.w = w


